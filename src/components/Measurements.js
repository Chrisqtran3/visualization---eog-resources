import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../store/actions";
import { Provider, createClient, useQuery } from "urql";

const client = createClient({
  url: "https://react.eogresources.com/graphql"
});

const query = `
query($input: [MeasurementQuery]) {
  getMultipleMeasurements(input: $input) {
    metric
    measurements {
      metric
      value
      at
      unit
    }
  }
}
`;

const getMetrics = state => {
  const { metrics } = state;
  const metricsArr = [];
  for (let i = 0; i < metrics.length; i++) {
    metricsArr.push({
      metricName: metrics[i]
    });
  }

  return {
    metrics: metricsArr
  };
};

export default () => {
  return (
    <Provider value={client}>
      <Measurements />
    </Provider>
  );
};

const Measurements = () => {
  const dispatch = useDispatch();

  const { metrics } = useSelector(getMetrics);
  const after = 1565117700451;
  const before = 1565117705453;
  const input = [];

  if (metrics.length > 0) {
    metrics.forEach(metric => {
      input.push({
        metricName: metric.metricName,
        after,
        before
      });
    });
  }

  const [result] = useQuery({
    query,
    variables: {
      input
    }
  });

  const { fetching, data, error } = result;

  useEffect(() => {
    if (error) {
      dispatch({ type: actions.API_ERROR, error: error.message });
      return;
    }
    if (!data) return;
    const { getMultipleMeasurements } = data;
    dispatch({ type: actions.MEASUREMENTS_RECEIVED, getMultipleMeasurements });
  }, [dispatch, data, error]);

  if (fetching) return <div>Fetching...</div>;

  const { getMultipleMeasurements } = data;

  const style = {
    width: "300px",
    height: "300px",
    margin: "20px auto",
    padding: "15px",
    border: "2px solid black",
    textAlign: "center"
  };

  return getMultipleMeasurements.map(({ metric, measurements }, index) => {
    return (
      <div style={style} key={index}>
        <h3>
          <strong>{metric}</strong>
        </h3>
        <p>{measurements[1].value}</p>
      </div>
    );
  });
};
