import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import * as actions from "../store/actions";
import { Provider, createClient, useQuery } from "urql";

const client = createClient({
  url: "https://react.eogresources.com/graphql"
});

const query = `
query {
  getMetrics
}
`;

export default () => {
  return (
    <Provider value={client}>
      <Metrics />
    </Provider>
  );
};

const Metrics = () => {
  const dispatch = useDispatch();

  const [result] = useQuery({
    query
  });

  const { error, data, fetching } = result;
  useEffect(() => {
    if (error) {
      dispatch({ type: actions.API_ERROR, error: error.message });
    }
    if (!data) return;
    const { getMetrics } = data;
    dispatch({ type: actions.METRICS_RECEIVED, getMetrics });
  }, [dispatch, data, error]);

  if (fetching) return <div>Fetching...</div>;

  return null;
};
