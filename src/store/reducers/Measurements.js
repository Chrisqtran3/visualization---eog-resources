import * as actions from "../actions";

const initialState = [];

const measurementsReceived = (state, action) => {
  const { getMultipleMeasurements } = action;

  return getMultipleMeasurements;
};

const handlers = {
  [actions.MEASUREMENTS_RECEIVED]: measurementsReceived
};

export default (state = initialState, action) => {
  const handler = handlers[action.type];
  if (typeof handler === "undefined") return state;
  return handler(state, action);
};
